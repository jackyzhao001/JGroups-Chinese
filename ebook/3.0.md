##3.	API   
本章说明了在JGroups中被用来构建可靠组通信的那些类，重点是创建和使用channels.   
本文档中的信息可能不是最新的，但这里面描述的JGroups的类本质上是相同的。最新的信息请参阅线上的javadoc文档。     
除非另行提到，在这里讨论的所有类都在org.jgroups包里面。    
##3.1. Utility classes    
org.jgroups.util.Util类里面包含了下面的常用功能，不能用在非Jgroups的包 。   
##3.1.1. objectToByteBuffer(), objectFromByteBuffer()    
第一个方法将对象作为参数并且把这个对象序列化到一个 byte buffer(这个对象必须实现serializable接口 ，或者externalizable接口，
或者Streamable接口)，返回byte数组。此方法通常用于将对象序列化到消息的字节缓冲区中。第二个方法从一个 buffer返回一个重建的对象。
两个方法如果没有实现serializable接口 ，或者externalizable接口，或者Streamable接口，那么都将抛出一个 exception。    

##3.1.2. objectToStream(), objectFromStream()    
第一个方法接收一个对象，并且把它写入到另一个 output stream中。第二个方法接收一个 input stream，并且从中读取一个对象。
两个方法如果没有实现serializable接口 ，或者externalizable接口，或者Streamable接口，那么都将抛出一个 exception。    

##3.2. Interfaces   
APIs会用到下面这些接口，因此它们首先被列出来。   
##3.2.1. MessageListener   
MessageListener  接口为消息接收提供了一个回调方法并且提供和设置状态。    
<pre class="brush: java; gutter: true;">
public interface MessageListener { 
	void receive(Message msg); 
	void getState(OutputStream output) throws Exception; 
	void setState(InputStream input) throws Exception; 
}
</pre>   
每当收到一条消息的时候，receive()方法被调用。getState() ， setState() 方法用于获取和设置group状态，(例如 每当加入group)。   
请参阅State transfer章节来讨论状态的转移。   
##3.2.2. MembershipListener(成员人数监听)   
MembershipListener接口跟上面MessageListener 接口类似。每次一个新 view，一个怀疑信息，或者一个阻塞事件被收到，
那么MembershipListener实现类相对应的方法会被调用.
<pre class="brush: java; gutter: true;">
	public interface MembershipListener { 
	void viewAccepted(View new_view); 
	void suspect(Object suspected_mbr); 
	void block(); 
	void unblock(); 
}
</pre>   
通常情况下，唯一需要实现的回调方法是viewAccepted。它当一个新的成员加入group或者一个现有的成员离开或者崩溃时通知消息的接收者。
JGroups如果怀疑每当有成员崩溃的时候就会调用suspect。but not yet excluded [1].   
调用block()方法来通知成员，它很快就会阻塞发送的消息。这是通过FLUSH协议完成的，比如一个状态转换或者设置view的时候确保没有成员能发送出消息。
当block()返回时，任何线程发送消息都将被阻塞。直到FLUSH再次解锁线程。例如状态成功转换之后。     
因此，block方法可用来发送等待的消息或者完成一些其他的工作。注意！block方法应该尽量的简单，否则整个FLUSH协议会被阻塞。   
unblock() 方法用来通知成员 FLUSH 协议已经完成，成员可以继续发送消息。如果成员没有停止用block()方法发送消息(消息正在发送中)，
FLUSH仅仅是被阻塞，如果block()方法结束后，FLUSH还会继续执行，所以成员不需要执行任何动作，实现unblock()方法也不是必须的。   
![](images/note.png)注意！通常扩展 ReceiverAdapter 且实现所需回调接口比实现这两种接口的所有方法简单。大多数的回调方法不是必须的。  
##3.2.4. ReceiverAdapter   
ReceiverAdapter 类用no-op(空实现)来实现Receiver。当这个类被调用，为了不实现这两种接口的所有方法，
我们可以简单的继承ReceiverAdapter，并且重写receive()方法,ReceiverAdapter 如下所示
<pre class="brush: java; gutter: true;">
public class ReceiverAdapter implements Receiver { 
	void receive(Message msg) {} 
	void getState(OutputStream output) throws Exception {} 
	void setState(InputStream input) throws Exception {} 
	void viewAccepted(View view) {} 
	void suspect(Address mbr) {} 
	void block() {} 
	void unblock() {} 
}
</pre>   
ReceiverAdapter类是被推荐的方式来实现回调方法。   
![](images/caution.png)注意！在回调方法中不能有任何可能发生阻塞的代码。
这包括发送消息；如果我们在栈顶上有FLUSH，然后在  viewAccepted()方法中发送一个消息，随后下面的事情会发生：  
FLUSH protocol 在创建安装一个view前阻塞所有的多播信息。然后创建一个view，然后解锁，可是，由于 view被创建，
就会触发 viewAcdepted()回调方法。viewAcdepted方法里面被发送的消息就会被锁定，这反过来又会阻塞  调用viewAccepted()的线程，
所以 flush 永远都不会返回结果。    
如果我们需要在回调方法中发送消息，那么应该在一个单独的线程中，或者提交到一个定时器中来执行。   

##3.2.5. ChannelListener
<pre class="brush: java; gutter: true;">
public interface ChannelListener {
	void channelConnected(Channel channel);
	void channelDisconnected(Channel channel);
	void channelClosed(Channel channel); 
}
</pre>   
一个实现了ChannelListener的类用Channel.addChannelListener()方法把一个channel注册进去，用来获取这个channel中状态变化的信息。 
每当 channel 关闭，断开，或者打开，那么与之相对应的回调方法会被调用。   
##3.3. Address
组内的每个成员都有一个地址。它唯一标识了成员。描述这种地址的接口是一个 Address。需要为这些方法提供具体的实现，
比如地址的比较和排序。JGroups addresses必须实现以下接口:
<pre class="brush: java; gutter: true;">
public interface Address extends Externalizable, Comparable, Cloneable {
	int size(); 
}
</pre>   
编组的目的，size()需要返回address实现了序列化形式的一个实例所占用的字节数    
![](images/warning.png)请不要直接使用Address的具体实现。Address应始终被当作集群节点的透明标识来使用。  
Addresses 的实现往往由最底层的协议层生成(比如：UDP 或者 TCP)，这允许JGroups使用addresses的所有可能的排序。
从一个地址唯一标识一个channel，group中的成员可以用addresses来把消息发送到该组的其他成员中，例如 在 Messages(见下节)。  

Address的默认实现是 org.jgroups.util.UUID.它唯一地标识节点。当断开并重新连接到集群，节点会被赋予一个新的UUID。
UUIDs 不会直接显示，但是会被当作一个逻辑名显示出来(see Logical names)。这个名字可以通过用户或者JGroups两者中的一个来赋予。
这么做的唯一目的是使日志输出更具可读性。    
UUIDs与 IpAddresses被设置为一对一的映射，IpAddresses是由IP 地址和端口号组成。他们最终会被传输协议用来发送message。    
##3.4. Message   
Data是在成员间以消息的形式发送(org.jgroups.Message).。成员可以发送一条消息给单个成员。
或者channel是一个 endpoint的所在组的所有成员。消息的结构是在Structure of a message.里面给出.     

![](images/Message.png)    

__Figure 3. Structure of a message__    
一条消息有5段组成：   
###Destination address(目标地址)   
接受者的地址。如果为null，则发送给当前组的所有成员。Message.getDest()返回消息的目标地址。   
###Source address   
发送者的地址。可以为空，这个地址会在消息发送到网络上之前被传输协议所填写(例如：UDP) 。
Message.getSrc()返回源地址，即 一个发送消息者的地址。   
###Flags   
它由一个 byte 组成，用来当作标识。目前公认的标识有：
OOB, DONT_BUNDLE, NO_FC,NO_RELIABILITY, NO_TOTAL_ORDER, NO_RELAY 和 RSVP. For OOB, 
see the discussion on the concurrent stack. For the use of flags see the message flags.   
###Payload(有效负载)   
实际数据(做为字节缓冲)。The Message class  包含一些方便的方法来设置一个可序列化的对象并再次检索该值。
从一个 byte buffer序列化为一个对象，或者把一个对象序列化到一个 byte buffer中去。
如果 buffer只是一个更大的buffer的子集，那么这个message还有一个偏移量和长度。   
###Headers   
Message可以附加一个headers的集合。任何代码都不应该放到可以被附加在message中做为header的Payload中。Message中的putHeader(), 
getHeader() and removeHeader()方法，可以用来操作headers。注意：headers只是被协议的实施者使用。
Headers不应该由应用程序代码来添加或者删除。    
一个 message 类似于一个IP数据包，由一个有效负载(一个 byte buffer)，发送方，接收方的地址组成(as Addresses)。
网络上的任何message 都可以被路由到相应的目的地(receiver address)，并且应答可以被返回到发送方的地址。   
当发送一个message时，通常不需要填写发送方的地址。这是在message被放到网络前由协议栈自动完成的。然而，在某些情况下，
message的发送方想要给定一个不同于自己的地址，比如：这个message的应答需要返回给其他成员。   
目标的地址(接收者)可以是一个Address类,用来指示成员的地址。例如：来自一个先前的message接收者地址，或者可以为 null，
这意味着该消息将被发送到该组的所有成员。一个典型的组播message，发送字符串”hello”到所有的成员需要像下面这样：  
<pre class="brush: java; gutter: true;">
Message msg=new Message(null, "Hello");
channel.send(msg);
</pre>  

##3.5. Header   
Header是一个自定义的bit信息，它可以被添加到每一个message中。JGroups广泛使用headers。
例如添加顺序号到每一个message（NAKACK and UNICAST）（NAKACK 确保消息的可靠和 FIFO 顺序），
使这些messages能按他们发送的顺序传递。   

##3.6. Event   
Events意味着可以借由JGroups协议实现彼此交谈。与message相反，它依次经过group成员之间的网络，events 只能在协议栈中上下传递。   
![](images/note.png)Headers 和 events 仅能被协议的实现者使用，不能在应用程序中使用它们。   
##3.7. View   
View(org.jgroups.View)用来表示本组成员的列表，它由一个ViewId组成。它唯一地标识了一个view（见下文），
和成员的列表。每当有新成员加入或者现有成员退出(或者崩溃)时，Views由channel底层协议栈自动生成。
group的所有成员都只能看到相同的views序列。   
注意，view中第一个成员是协调器（the one who emits new views），因此，每当成员身份改变，
每个成员都能很容易地找到协调器，无需联系其他成员，那就是选择view的第一个成员。   
下面的代码演示了如何发送（单播）消息到第view的第一个成员（省略检查错误的代码）:   
<pre class="brush: java; gutter: true;">
View view=channel.getView();
Address first=view.getMembers().get(0); 
Message msg=new Message(first, "Hello world"); 
channel.send(msg);
</pre>   
每当新的view被生成，应用程序被通知，比如：Receiver.viewAccepted()，view在channel中已经被设定。  
例如：在viewAccepted()回调方法中调用Channel.getView()将会返回相同的view。（或可能是下一个不同的view，以防已经生成了新的view！）   
##3.7.1. ViewId   
ViewId用来标识唯一的view，它由view创建者的地址和一个序列号构成。
ViewIds可以比较是否相等，如果实现了 equals() and hashCode()方法，还可以放入 hashmaps 中。   
![](images/note.png)注意，后者的2个方法仅仅需要把ID考虑进去。   
##3.7.2. MergeView   
当group拆分成多个子groups的时候,例如:由于网络分区的原因，而后这些子groups又重新合并。
应用程序接收替代了View的MergeView。该MergeView是View的子类，并且包含了一个额外的变量，
该变量持有views列表合并后的值。看个例子， view V1:(p,q,r,s,t)表示一个group，
被分成了多个子group V2:(p,q,r) 和 V2:(s,t), 合并后的view可能是V3:(p,q,r,s,t).
这种情况下，MergeView 包含了由2个 views  V2:(p,q,r) ，V2:(s,t)组成的集合。   

##3.8. JChannel   
为了要加入group来发送messages，进程创建了channel。Channel就像是socket，当客户端连接到channel，
客户端给定想要加入的group名，因此，channel（在其连接状态下的）总是与特定的group相关联。
protocol stack确保有相同group名的channels能找到彼此，每当客户端连接到一个给定group名为G的channel，
然后它会尝试找到具有相同名称的现有channels，并加入它们。最终，一个新的view会产生（包含新的member），
如果不存在members，将创建一个新的group。   
下图是一个channel状态转换示意图，   
![](images/ChannelStates.png)   
当channel第一次被创建，它处于未被连接的状态，如果channel在未连接状态下试图执行某些特定的任务(例如，发送/接收 messages) 
将会导致异常。在客户端成功连接之后，它会转换为已连接状态，现在channel可以从其他members接收messages，
也可以发送messages给其他的members或者group，并且当有新成员加入或离开时，它都会得到通知。
获得channel的本地地址可以保证在这一状态下正确运行。    
当channel已经断开连接，它会转换为未连接的状态。已连接状态，未连接状态，两者都可以被关闭，会使channel进一步的操作变为不可用，
任何尝试都会产生异常，当channel直接从连接状态关闭，首先会断开连接，然后才关闭。    

现在会讨论一些用于创建和操作channel的方法。   

##3.8.1. Creating a channel   
channel使用其中一个公有的构造方法来创建(e.g. new JChannel()).   
最常用的JChannel的构造方法如下所示:   
<pre>public JChannel(String props) throws Exception;</pre>    
props参数指向了一个XML文件，它包含了要使用的协议栈的配置，props可以是一个字符串，
也可以用其他的方式构造，例如，一个dom元素或者URL（更多的详细信息参看javadoc）。   
下面的代码示例演示了怎样创建一个基于XML配置文件的channel:   
<pre>JChannel ch=new JChannel("/home/bela/udp.xml");</pre>  
如果props参数是null，将使用默认属性，如果无法创建channel，将抛出异常。未发现协议，
或者错误的协议参数也是使channel无法创建的可能原因。例如，Draw例子可以像下面这样启动：   
<pre>java org.javagroups.demos.Draw -props file:/home/bela/udp.xml</pre>  
或者   
<pre>java org.javagroups.demos.Draw -props http://www.jgroups.org/udp.xml</pre>    
后一种情况下，应用程序会从服务器上面下载其协议栈的规范。它考虑到应用程序的properties集中式管理的需求。   
简单的XML配置文件像下面这样（编辑自udp.xml）.    
<pre><code>
&lt;config xmlns="urn:org:jgroups"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="urn:org:jgroups http://www.jgroups.org/schema/jgroups.xsd"&gt;
    &lt;UDP
         mcast_port="${jgroups.udp.mcast_port:45588}"
         ucast_recv_buf_size="20M"
         ucast_send_buf_size="640K"
         mcast_recv_buf_size="25M"
         mcast_send_buf_size="640K"
         loopback="true"
         discard_incompatible_packets="true"
         max_bundle_size="64K"
         max_bundle_timeout="30"
         ip_ttl="${jgroups.udp.ip_ttl:2}"
         enable_diagnostics="true"

         thread_pool.enabled="true"
         thread_pool.min_threads="2"
         thread_pool.max_threads="8"
         thread_pool.keep_alive_time="5000"
         thread_pool.queue_enabled="true"
         thread_pool.queue_max_size="10000"
         thread_pool.rejection_policy="discard"

         oob_thread_pool.enabled="true"
         oob_thread_pool.min_threads="1"
         oob_thread_pool.max_threads="8"
         oob_thread_pool.keep_alive_time="5000"
         oob_thread_pool.queue_enabled="false"
         oob_thread_pool.queue_max_size="100"
         oob_thread_pool.rejection_policy="Run"/&gt;

    &lt;PING timeout="2000"
            num_initial_members="3"/&gt;
    &lt;MERGE3 max_interval="30000"
            min_interval="10000"/&gt;
    &lt;FD_SOCK/&gt;
    &lt;FD_ALL/&gt;
    &lt;VERIFY_SUSPECT timeout="1500"  /&gt;
    &lt;BARRIER /&gt;
    &lt;pbcast.NAKACK use_stats_for_retransmission="false"
                   exponential_backoff="0"
                   use_mcast_xmit="true"
                   retransmit_timeout="300,600,1200"
                   discard_delivered_msgs="true"/&gt;
    &lt;UNICAST timeout="300,600,1200"/&gt;
    &lt;pbcast.STABLE stability_delay="1000" desired_avg_gossip="50000"
                   max_bytes="4M"/&gt;
    &lt;pbcast.GMS print_local_addr="true" join_timeout="3000"
                view_bundling="true"/&gt;
    &lt;UFC max_credits="2M"
         min_threshold="0.4"/&gt;
    &lt;MFC max_credits="2M"
         min_threshold="0.4"/&gt;
    &lt;FRAG2 frag_size="60K"  /&gt;
    &lt;pbcast.STATE_TRANSFER /&gt;
&lt;/config&gt;
</code></pre>

这个协议栈是被<config>和</config>元素包裹并且罗列了从底层（UDP）到顶层（STATE_TRANSFER）所有的协议，每个元素都定义了一个协议。   
每一个协议都被实现为一个java类，当协议栈基于上面的XML配置创建时，第一个元素（“UDP”）成为最底层的协议，
第二个元素名列第一个元素下面，以此类推从底层到顶层。    
每一个元素名称相对应的java类都包含在org.jgroups.protocols的包中。注意：仅仅是类的简单名称，
不是全限定类名（UDP代替org.jgroups.protocols.UDP)。如果协议栈的class没有发现，JGroups 假定给定的名称是一个全限定类名，
因此会尝试着实例化该类，如果不能工作，会抛出异常。这允许协议类可以完全存放在不同的（第三方）包中，
例如，有效的协议名可能是com.sun.eng.protocols.reliable.UCAST.    
每层都可能有0个或多个参数，它们直接在尖括号后的协议名中做为键值对集合被指定。在上面的例子中，UDP使用一些配置项，
其中一个IP多播端口（mcast_port）设置为45588.或者如果设置了系统属性jgroups.udp.mcast_port，就会使用它。   
![](images/note.png)注意，组内所有成员都必须使用相同的协议栈。   

###Programmatic creation（以编程的方式创建）   
通常，channels是通过传递一个XML配置文件的名称给JChannel()的构造器来创建，这个声明式的配置的顶层，
JGroups提供了一个API以编程的方式创建channel。    
该方法首先创建一个JChannel，然后实例化一个ProtocolStack，然后添加所有需求的协议到堆栈里，
最后调用init()方法在堆栈上面来设置它。剩下的，例如，调用JChannel.connect() 与声明式的创建方式是一样的。   
下面是一个示例，说明如何以编程方式创建一个channel（从ProgrammaticChat复制）。    
<pre class="brush: java; gutter: true;">
public class ProgrammaticChat {

    public static void main(String[] args) throws Exception {
        JChannel ch=new JChannel(false);         // (1)
        ProtocolStack stack=new ProtocolStack(); // (2)
        ch.setProtocolStack(stack);
        stack.addProtocol(new UDP().setValue("bind_addr",
                                              InetAddress.getByName("192.168.1.5")))
                .addProtocol(new PING())
                .addProtocol(new MERGE3())
                .addProtocol(new FD_SOCK())
                .addProtocol(new FD_ALL().setValue("timeout", 12000)
                                         .setValue("interval", 3000))
                .addProtocol(new VERIFY_SUSPECT())
                .addProtocol(new BARRIER())
                .addProtocol(new NAKACK())
                .addProtocol(new UNICAST2())
                .addProtocol(new STABLE())
                .addProtocol(new GMS())
                .addProtocol(new UFC())
                .addProtocol(new MFC())
                .addProtocol(new FRAG2());       // (3)
        stack.init();                            // (4)

        ch.setReceiver(new ReceiverAdapter() {
            public void viewAccepted(View new_view) {
                System.out.println("view: " + new_view);
            }

            public void receive(Message msg) {
                Address sender=msg.getSrc();
                System.out.println(msg.getObject() + " [" + sender + "]");
            }
        });

        ch.connect("ChatCluster");


        for(;;) {
            String line=Util.readStringFromStdin(": ");
            ch.send(null, line);
        }
    }

}
</pre>   
首先创建JChannel（1）。参数 false 告诉channel不去创建 ProtocolStack，这是必要的，因为稍后我们会亲自创建它，
并且在channel中设置它（2）。   
下一步，所有的protocols都会被添加到stack（3）。注意，该命令是从底部（传输协议）到顶部。所以UDP做为传送层协议被首先添加进去。
然后是PING 等等。一直到FRAG2这个顶层协议。每个协议都可以通过setters方法配置，但是也有一个通用的方法 
setValue(String attr_name, Object value),这同样也可以用来配置协议栈，如下所示：   
一旦配置了堆栈，我们就可以调用ProtocolStack.init()来正确的连接所有的协议并且调用每个协议实例的init()方法(4).在此之后，
channel可以使用并且所有的后续操作(例如connect())都可以执行了。当init()方法返回，
我们实质上已经拥有相当于新的JChannel(config_file)。   

##3.8.2. Giving the channel a logical name   
可以给予channel一个逻辑名，用来在toString()方法中代替channel的地址。逻辑名可以显示channel的功能，例如, "HostA-HTTP-Cluster", 
它比一个  UUID  3c7e52ea-4087-1859-e0a9-77a0d2f69f29. 表达的更清晰易懂。     
例如，当我们有3个channel，使用了逻辑名后，我们可能会看到类似像 view{A,B,C｝，
这可比{56f3f99e-2fc0-8282-9eb0-866f542ae437,ee0be4af-0b45-8ed6-3f6e-92548bfa5cde, 9241a071-10ce-a931-f675-ff2e3240e1ad}漂亮多了！   
如果未设置逻辑名，JGroups会生成一个，使用主机名和一个随机数，例如，linux-3442.，如果你不需要设置逻辑名，还想显示UUIDs的话，
可以使用系统属性-Djgroups.print_uuids=true 来设置。   
逻辑名可以用如下方式设置：    
<pre class="brush: java; gutter: true;">public void setName(String logical_name);</pre>   
在连接channel之前，必须设置setName。注意，逻辑名是和channel并驾齐驱的，直到channel被销毁，鉴于在每个连接上创建UUID。   
当JGroups开始的时候，它会打印逻辑名和相关的物理地址：    
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-      
GMS: address=mac-53465, cluster=DrawGroupDemo, physical address=192.168.1.3:49932   
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-      
逻辑名是 mac-53465,物理地址是192.168.1.3:49932，UUID 在这里没有显示。   
##3.8.3. Generating custom addresses（生成自定义地址）    
自2.12后，地址生成就是可插拔的了。这意味着应用程序可以决定它使用什么样的地址。默认地址类型是UUID，由于某些协议使用UUID，
所以建议提供自定义的类做为UUID的子类。这可以用于通过地址传递额外的数据，这些数据可以是，该地址被分配到的相关节点的位置。 
注意，UUID父类的equals(), hashCode() , compare() 方法不需要改变。   
使用自定义地址，org.jgroups.stack.AddressGenerator的实现类必须重写。   
任何CustomAddress类，为了正确的控制，需要用ClassConfigurator 注册。   
<pre class="brush: java; gutter: true;">
class CustomAddress extends UUID {
    static {
        ClassConfigurator.add((short)8900, CustomAddress.class);
    }
}
</pre>
![](images/note.png)注意，选择的ID不能与jg-magic-map.xml.中其他的IDs相冲突。   
JChannel中设置地址生成器：setAddressGenerator(AddressGenerator).在channel连接之前，这个必须要先设置。
子类的一个列子是org.jgroups.util.PayloadUUID, jGroups中附带的实现超过2个。   
##3.8.4. Joining a cluster（加入集群）    
当client想要加入集群，client需要通过要加入群集的名称来连接channel ：   
<pre class="brush: java; gutter: true;">public void connect(String cluster) throws Exception;</pre>    
集群名称是client想要加入的集群的名称。用相同名称来调用connect()的所有channel形成一个集群。
在同一个集群中的任何channel上发送的Messages都将被所有的成员接收（包括发送Message的那个channel）。    
![](images/note.png)可以用setDiscardOwnMessages(true).来关闭本地交付，即自己发送的消息，自己不接收。
已成功加入到集群后connect()方法会尽快返回。如果channel处于关闭的状态（see channel states），
会抛出异常。如果没有其他成员，也就是说，没有其他成员用这个相同的名称来连接到这个集群中，
这时，就会创建一个新的集群并且这个成员会做为第一个加入的成员。这个集群的第一个成员就会充当协调器。
每当成员身份变动时，协调器负责安装新的views。     
##3.8.5. Joining a cluster and getting the state in one operation（加入一个集群并且获取一个操作中的状态）     
客户端也可以加入集群且在一个操作中获取集群的状态。概念上来说，连接和获取状态的最佳方式是将它想象成一个常规调用connect()和getState()方法来连续地执行。
但是，使用connect和获得state connect连接方法有几个优势，超过了常规的connect，首先，
底层message交换是有比较大的优化的，特别是如果使用了flush protocol ，但更重要的是，从client的角度，
connect()和获取状态操作变成了一个原子操作。    
<pre class="brush: java; gutter: true;">public void connect(String cluster, Address target, long timeout) throws Exception;</pre> 
这个普通的 connect()方法，参数cluster做为名称表示要加入一个集群，参数target 指出集群成员应该从哪里获取状态，
如果target为null，那就是说会从集群协调器中获取状态，如果状态应该从协调器以外的给定成员上获取，客户端只需提供该成员的地址，
timeout参数限定了整个连接和获取状态的操作的超时时间。如果超时，会抛出异常。     

##3.8.6. Getting the local address and the cluster name    
方法getAddress()返回channel的地址。当一个channel处于非连接的状态，address有可能能用，也有可能不能用。   
<pre class="brush: java; gutter: true;">public Address getAddress();</pre> 
方法getClusterName()返回成员加入集群的名字。   
<pre class="brush: java; gutter: true;">public String getClusterName();</pre> 
再者，如果channel处于断开或者关闭的状态时，getClusterName()返回的结果是undefined。   
##3.8.7. Getting the current view   
下面的方法可用于获得当前channel的view：   
<pre class="brush: java; gutter: true;">public View getView();</pre> 
此方法返回当前channel的view。每次安装一个新view的时候View里面的成员都会更新(channel中回调viewAccepted()来实现).。
在未连接或者已经关闭了的channel上调用这个实现类已经定义了的方法。channel可以返回null，或可能返回它知道的上一个view。    

##3.8.8. Sending messages   
一旦通道已经连接，可以使用一个send()方法来发送messages。   
<pre class="brush: java; gutter: true;">
public void send(Message msg) throws Exception; 
public void send(Address dst, Serializable obj) throws Exception; 
public void send(Address dst, byte[] buf) throws Exception; 
public void send(Address dst, byte[] buf, int off, int len) throws Exception;
</pre>    
第一个send()方法仅仅有一个参数，只是发送message。message的目标应该是接收者的地址（单播）或者是null（多播）这两者之一。
当目标是null，该消息将被发送到集群中所有的成员（包括自己）。    
剩下的那些send()方法都是些帮助方法。它们接收byte[] buffer或可被序列化对象两者中的一者。创建Message并调用send(Message).    
如果channel没连接或已经关闭了，那么在这种情况下尝试发送message会抛出异常。   
下面的示例将消息发送到集群的所有成员：   
<pre class="brush: java; gutter: true;">
Map data; // any serializable data 
channel.send(null, data);
</pre>    
把null值当作目标地址的意思是message会发送到所有的成员中。有效载荷是一个hashmap，它会被序列化到message的缓冲区并且在接收端解序列化。
另外，生成byte buffer的任何其他方式和向它设置message buffer(例如，使用Message.setBuffer()) 同样适用。    
下面是一个示例发送单播消息给组中的第一个成员(协调器):    
<pre class="brush: java; gutter: true;">
Map data; 
Address receiver=channel.getView().getMembers().get(0); 
channel.send(receiver, "hello world");
</pre>     
示例代码要向（view的第一个成员）发送“hello world”message。   

###Discarding one’s own messages（丢弃自己的消息）     
有时，不必处理自己的消息(发送给集群的消息，自己同样会收到) ，即，发送给自己的message。
为此，JChannel.setDiscardOwnMessages(boolean flag)设置成true，默认是false。这意味着每个集群节点都将收到一条由P发送的message，
但是P自身不会收到。    
注意，这个方法替代了旧方法JChannel.setOpt(LOCAL, false)，旧方法在3.0中被移除了。    

###Synchronous messages（消息同步）    
尽管JGroups保证message最终会在所有的正常成员中交付，有时，这会耽误一些时间，例如，如果我们有一个基于negative acknowledgments
（否定应答：错误则返回不确定）【acknowledgments (ACKs) and negative acknowledgments (NACKs or STATIC-NACKs)】的重传协议，
并且最后发送的message丢失了，然后接收者（一些接受者）在message重发之前将不得不等待，直到stability protocol通知message已经丢失。   
这可以在message中通过设置Message.RSVP 标志来更改：当遇到此标志，message会发送blocks直到所有成员都承认接收到了消息
(当然不包括崩溃的或者同一时刻离开的成员).     
另一个目的：如果我们发送了一个由RSVP-tagged 设置的消息, 接着当send()方法返回时，我们确保所有的消息发送后都会在所有的成员中交付。
所以，例如，如果成员P发送多条消息1-10，并且把第10条消息标记为RSVP，然后，在JChannel.send()返回时，
P就知道所有的成员都已经从P接收到了1-10消息了。    
注意由于RSVP标记的message是昂贵的，并且可能会阻塞发送者一段时间，使用它时需谨慎。例如，当完成一个工作单元
（即，成员P发送messages N），并且P需要知道所有的信息都被每一个成员收到，那么就可以使用RSVP。    
使用RSVP,要做的2件事：    
首先，RSVP协议必须被配置，使用如NAKACK 或 UNICAST(2)的一些协议 如下：    
<pre><code>
&lt;config&gt;
    &lt;UDP/&gt;
    &lt;PING /&gt;
    &lt;FD_ALL/&gt;
    &lt;pbcast.NAKACK use_mcast_xmit="true"
                   discard_delivered_msgs="true"/&gt;
    &lt;UNICAST timeout="300,600,1200"/&gt;
    &lt;RSVP /&gt;
    &lt;pbcast.STABLE stability_delay="1000" desired_avg_gossip="50000"
                   max_bytes="4M"/&gt;
    &lt;pbcast.GMS print_local_addr="true" join_timeout="3000"
                view_bundling="true"/&gt;
    ...
&lt;/config&gt;
</code></pre>

其次，我们想要得到的message的 ack`ed（确认字符）就必须标记为RSVP：   
<pre class="brush: java; gutter: true;">
Message msg=new Message(null, null, "hello world"); 
msg.setFlag(Message.RSVP); 
ch.send(msg);
</pre>  
在这里，我们发送消息到集群中的所有成员（dest==null）。（注意，RSVP也可以用于将消息发送到一个单播的目的地）。
方法send（）将会尽快返回从所有当前成员接收到的ack（确认字符）。如果有4个成员，A，B，C和D，并且A从自身收到acks，
还收到从B的，C的acks，但D的ask丢失和崩溃，在D被超时踢出之前，仍将使send()返回，就跟D实际上发送了ack一样。     
如果timeout属性大于0，且我们在超时毫秒内没有收到所有的acks，将抛出TimeoutException异常（
如果RSVP.throw_exception_on_timeout是true） ，应用程序可以选择性的抓住这个（运行时）异常，并且对其进行某些操作，例如，重试！   
RSVP配置的描述在这里：ＲＳＶＰ   
![](images/note.png)RSVP被添加到3.1版本中。   

###Non blocking RSVP(非阻塞RSVP)    
有时发送者想要重新发送给定的message，到它已经被收到为止，或者出现了超时，但是不想它发生阻塞。有一个列子，
RpcDispatcher.callRemoteMethodsWithFuture()方法需要立即返回，即便结果还不可用。如果调用的选项包含标志RSVP，
那么将来只有等收到所有的响应后才能返回，这显然是不可取的行为。    
为了解决这个问题，可以使用RSVP_NB(non-blocking)标志，它和RSVP有相同的行为，但是调用者不会被RSVP协议所阻塞。当发生超时，
会记录一个警告message，但由于调用者不会被阻塞，所以调用不会引发异常。    

##3.8.9. Receiving messages（接收消息）    
ReceiverAdapter (或Receiver)中的方法receive()可以被重写用以接收messages，views，和state transfer callbacks.。    
public void receive(Message msg);    
Receiver 可以被JChannel.setReceiver()方法注册到channel中，所有收到的massages，view 的变更，
状态传输请求（state transfer requests）都将调用已注册的Receiver的回调方法。    
<pre class="brush: java; gutter: true;">
JChannel ch=new JChannel();
ch.setReceiver(new ReceiverAdapter() {
    public void receive(Message msg) {
        System.out.println("received message " + msg);
    }
    public void viewAccepted(View view) {
        System.out.println("received view " + new_view);
    }
});
ch.connect("MyCluster");
</pre>    

##3.8.10. Receiving view changes（接收view变更）   
如上所示，每当集群成员身份发生变更的时候，ReceiverAdapter的回调方法viewAccepted()就可以用于获取回调了。
Receiver需要通过设置JChannel.setReceiver(Receiver)。    
As discussed in ReceiverAdapter,回调中必须避免任何需要很多执行时间的代码，更不能发生阻塞。   
JGroups 调用的这个回调是view安装的一部分，如果这个用户的代码发生阻塞，那么view的安装也就会阻塞。    

##3.8.11. Getting the group’s state   
新加入的成员在开始工作之前可能检索当前集群的状态，它通过getState()实现：   
<pre class="brush: java; gutter: true;">public void getState(Address target, long timeout) throws Exception;</pre>  
此方法返回一个成员的状态（通常是最老的成员，也就是协调器），通常情况下target参数为null，就是询问当前协调器的状态，
如果超过设定的timeout（毫秒）还没有获取到状态，会抛出异常，timeout设置为0，则将会一直等待，直到整个状态完成传输。
    
不直接把返回状态作为getState()的结果的原因（getState方法是void）是,相对于其他messages必须在正确的位置返回.
直接返回状态将会违反channel的FIFO(先进先出)属性，并且状态的传输也可能不正确。    
参与状态传输中，state 的提供者和state 的请求者这两者都必须实现ReceiverAdapter (Receiver)中的回调方法:
<pre class="brush: java; gutter: true;">
public void getState(OutputStream output) throws Exception;
public void setState(InputStream input) throws Exception;
</pre>  
方法getState()被state的提供者调用（通常是协调器），它需要把state写入到给定的输出流中，
注意当输出流完成时是不需要关闭的（或者当抛出异常时），这由JGroups完成。     
setState() 方法被state的请求者调用，这是调用JChannel.getState()的那个成员。它需要从 input stream中读出state并且为其设置内部状态。
同样，它也不需要关闭。   
在一个由A，B，C组成的集群中，D加入了集群并且调用了Channel.getState()，会发生下面的调用次序:   

+ D调用JChannel.getState().这会从老成员A中取回state。    
+ A的回调方法getState()被调用，A将其状态写入到输出流中做为参数传递给getState()。   
+ D的setState()把输入流当作一个参数来进行回调调用。D从输入流中读取状态并且为其设置内部状态，覆盖先前的任何数据。   
+ D调用的JChannel.getState()返回。注意这仅在state成功传输之后才能反生，或者超时过期，或者其他的state提供者或者请求者抛出异常，
这种异常将被getState()再次抛出。这可能发生，例如如果state的提供者的回调方法getState()试图把一个非可序列化的类序列化到
output stream中。    
 
下面的代码片段显示了组内成员如何参与状态的传输：    
<pre class="brush: java; gutter: true;">
public void getState(OutputStream output) throws Exception {
    synchronized(state) {
        Util.objectToStream(state, new DataOutputStream(output));
    }
}

public void setState(InputStream input) throws Exception {
    List<String> list;
    list=(List<String>)Util.objectFromStream(new DataInputStream(input));
    synchronized(state) {
        state.clear();
        state.addAll(list);
    }
    System.out.println(list.size() + " messages in chat history):");
    for(String str: list)
        System.out.println(str);
}
</pre>  
此代码是JGroups教程中的聊天示例并且这里的state是strings的一个集合。   
该getState() 方法在state上执行了同步操作。所以没有刚传入的消息能在state传输中修改它，并使用JGroups的工具方法objectToStream().   

###Performance when writing to an output stream（写入output stream的性能）    
如果很多小碎片被写入到输出流时，最好包装一个output stream到BufferedOutputStream中。例如，    
<pre class="brush: java; gutter: true;">
Util.objectToStream(state,
                    new BufferedOutputStream(
                        new DataOutputStream(output)));
</pre>  
setState()方法的实现也使用了Util.objectFromStream()这个实用方法来从input stream中读取状态并且把它分配给其内部列表。    

###State transfer protocols  
为了能使状态进行传输，一个状态传输协议必须包括在配置中，这可以是STATE_TRANSFER, STATE, 或 STATE_SOCK.。
协议的更多细节可以在协议列表部分找到。   

__STATE_TRANSFER__   
这是原始状态传输协议，用于传输byte[]缓冲区,表面如此，但是内部转换成输入输出流来调用回调方法 getState() 和 setState()。    
注意，由于byte[]缓冲区转换为输入输出流，所以本协议不能用来传输大的state。    
详情见pbcast.STATE_TRANSFER.    

__STATE__    
 STREAMING_STATE_TRANSFER 协议, 重命名于 3.0版本. 它发送完整的state覆盖从提供者到请求者的整块(可配置的)区域，所以内存消耗最小。   
详情见pbcast.STATE.   
__STATE_SOCK__   
同STREAMING_STATE_TRANSFER一样，但是在提供者到请求者之间使用TCP连接来传输状态。   
详情见STATE_SOCK.   

##3.8.12. Disconnecting from a channel   
断开channel连接使用以下方法：   
public void disconnect();   
如果channel已经断开或关闭，那么它将没有任何效果。如果在连接中，它会离开集群，它通过发送一个离开请求到当前的协调器
（对于channel使用者来说是透明的）。协调器随后会从当前的view删除离开的节点并且在所有剩下的成员中安装生成一个新的view。   
成功断开连接后，channel处于未连接的状态且可以随时重新建立连接。   

##3.8.13. Closing a channel   
销毁channel实例（销毁相关的协议栈，并且释放所有的资源），使用close()方法。   
public void close();   
关闭一个连接着的channel前，应该首先断开连接。   
close()方法把channel的状态变为closed，其后的操作都是不允许的（在一个已经关闭的channel上调用方法时更多的会抛出异常），
在这种状态下，channel实例不考虑再由应用程序使用-当channel实例对象的句柄被重置时-之前的channel实例由于失去句柄，
只能呆在java堆中，直到被 runtime system垃圾回收。  


## links
   * [目录](<directory.md>)
   * 上一节: [Unsupported classes ](<2.3.md>)
   * 下一节: [Building Blocks](<4.0.md>)





































   

